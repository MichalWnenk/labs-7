﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Media;
using Lab7.AddressControl.Contract;

namespace Lab7.AddressControl
{
    public class SimpleViewModel : ViewModel
    {
        private Command komendaZatwierdz;
        private string url;
        private ImageSource image;

        public SimpleViewModel()
        {
            komendaZatwierdz = new Command(Zatwierdz);
            this.url = "";
            this.image = null;
        }

        public string AddressChanged
        {
            get { return url; }
            set
            {
                url = value;
                FirePropertyChanged("AddressChanged");
            }
        }

        public ImageSource Image
        {
            get { return image; }
            set
            {
                image = value;
                FirePropertyChanged("Image");
            }
        }

        public Command KomendaZatwierdz { get { return komendaZatwierdz; } }

        public void Zatwierdz(object parameter)
        {
            MemoryStream ms = new MemoryStream(new WebClient().DownloadData(new Uri(this.url)));
            ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            Image = (ImageSource)imageSourceConverter.ConvertFrom(ms);
            MessageBox.Show("Obrazek wgrany! :D");
        }
    }
}
